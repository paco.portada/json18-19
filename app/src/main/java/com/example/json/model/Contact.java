package com.example.json.model;

public class Contact {
    private String name;
    private String address;
    private String email;
    private Phone phone;
    public String getName() { return name; }
    public void setName(String name) {
        this.name = name;
    }
    public String getAddress() { return address; }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getEmail() { return email; }
    public void setEmail(String email) {
        this.email = email;
    }
    public Phone getPhone() { return phone; }
    public void setPhone(Phone t) {
        this.phone = t;
    }
    public String toString() {
        return name;
    }
}
