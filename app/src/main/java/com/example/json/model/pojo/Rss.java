
package com.example.json.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rss {

    @SerializedName("xmlns:atom")
    @Expose
    private String xmlnsAtom;
    @SerializedName("channel")
    @Expose
    private Channel channel;
    @SerializedName("version")
    @Expose
    private String version;

    public String getXmlnsAtom() {
        return xmlnsAtom;
    }

    public void setXmlnsAtom(String xmlnsAtom) {
        this.xmlnsAtom = xmlnsAtom;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

}
