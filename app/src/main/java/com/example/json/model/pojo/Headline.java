package com.example.json.model.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Headline {
    private String web;
    private String feed;
    @SerializedName("titles")

    private List<Item> headlines;
    public String getWeb() {
        return web;
    }
    public void setWeb(String web) {
        this.web = web;
    }
    public String getFeed() {
        return feed;
    }
    public void setFeed(String feed) {
        this.feed = feed;
    }
    public List<Item> getHeadlines() {
        return headlines;
    }
    public void setHeadlines(List<Item> headlines) {
        this.headlines = headlines;
    }
}
