
package com.example.json.model.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Item {

    @SerializedName("enclosure")
    @Expose
    private Enclosure enclosure;
    @SerializedName("link")
    @Expose
    private String link;
    @SerializedName("description")
    @Expose
    private String description;
    //@SerializedName("guid")
    //@Expose
    //private Guid guid;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("category")
    @Expose
    //private String category;
    private List<Category> category;
    @SerializedName("pubDate")
    @Expose
    private String pubDate;

    public Enclosure getEnclosure() {
        return enclosure;
    }

    public void setEnclosure(Enclosure enclosure) {
        this.enclosure = enclosure;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

//  public Guid getGuid() {
//        return guid;
//    }
//
//    public void setGuid(Guid guid) {
//        this.guid = guid;
//    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

}
