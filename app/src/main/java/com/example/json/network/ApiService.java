package com.example.json.network;

import com.example.json.model.Repo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiService {
    @GET("users/{username}/repos")
    Call<ArrayList<Repo>> listRepos(@Path("username") String username);

    @GET("acceso/repositorios.json")
    Call<ArrayList<Repo>> getRepos();
}
