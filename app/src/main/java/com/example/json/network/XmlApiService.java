package com.example.json.network;

import com.example.json.model.RSSFeed;

import retrofit2.Call;
import retrofit2.http.GET;

public interface XmlApiService {
    @GET("rss/rss.aspx?ch=00279")
    Call<RSSFeed> loadRss();
}
