package com.example.json;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.json.ui.ContactsActivity;
import com.example.json.ui.GsonContactsActivity;
import com.example.json.ui.LotteryActivity;
import com.example.json.ui.NewsActivity;
import com.example.json.ui.RepositoriesActivity;
import com.example.json.ui.XmlAndJsonCreationActivity;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @OnClick({R.id.button1, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6})
    public void submit(View view) {
        Intent i = null;

        switch (view.getId()){
            case R.id.button1:
                i = new Intent(this, LotteryActivity.class);
                break;
            case R.id.button2:
                i = new Intent(this, ContactsActivity.class);
                break;
            case R.id.button3:
                i = new Intent(this, GsonContactsActivity.class);
                break;
            case R.id.button4:
                i = new Intent(this, RepositoriesActivity.class);
                break;
            case R.id.button5:
                i = new Intent(this, NewsActivity.class);
                break;
            case R.id.button6:
                i = new Intent(this, XmlAndJsonCreationActivity.class);
                break;
        }
        startActivity(i);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
}
