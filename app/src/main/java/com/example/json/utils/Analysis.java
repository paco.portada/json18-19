package com.example.json.utils;

import android.os.Environment;
import android.util.Log;
import android.util.Xml;

import com.example.json.model.Contact;
import com.example.json.model.Phone;
import com.example.json.model.pojo.Headline;
import com.example.json.model.pojo.Item;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class Analysis {
    public static String lotteryAnalyze(JSONObject object) throws JSONException {
        JSONArray jsonArray;
        JSONObject item;
        String information;
        StringBuilder text = new StringBuilder();

        information = object.getString("info");
        jsonArray = object.getJSONArray("sorteo");
        text.append("Sorteos de la Primitiva:\n");
        for (int i = 0; i < jsonArray.length(); i++) {
            item = jsonArray.getJSONObject(i);
            text.append(information + ": " + item.getString("fecha") + '\n');
            text.append(item.getInt("numero1") + ", " + item.getInt("numero2") + ", " + item.getInt("numero3") + ", ");
            text.append(item.getInt("numero4") + ", " + item.getInt("numero5") + ", " + item.getInt("numero6") + '\n');
            text.append("Complementario: " + item.getInt("complementario") + ", Reintegro: " + item.getInt("reintegro") + '\n');
        }
        return text.toString();
    }

    public static ArrayList<Contact> contactsAnalyze(JSONObject response) throws JSONException {
        JSONArray jsonArray;
        JSONObject contactObject, phoneObject;
        Contact contact;
        Phone phone;
        ArrayList<Contact> contacts = new ArrayList<>();
        // añadir contactos (en JSON) a people
        jsonArray = response.getJSONArray("contactos");
        for (int i = 0; i < jsonArray.length(); i++) {
            contactObject = jsonArray.getJSONObject(i);
            contact = new Contact();
            contact.setName(contactObject.getString("nombre"));
            contact.setAddress(contactObject.getString("direccion"));
            contact.setEmail(contactObject.getString("email"));
            phoneObject = contactObject.getJSONObject("telefono");
            phone = new Phone();
            phone.setHome(phoneObject.getString("casa"));
            phone.setMobile(phoneObject.getString("movil"));
            phone.setWork(phoneObject.getString("trabajo"));
            contact.setPhone(phone);
            contacts.add(contact);
        }
        return contacts;
    }

    public static void createXML(List<Item> news, String file) throws IOException {
        FileOutputStream fout;
        fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), file));
        XmlSerializer serializer = Xml.newSerializer();
        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        //poner tabulación
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);

        serializer.startTag(null, "titulares");
        for (int i = 0; i < news.size(); i++) {
            serializer.startTag(null, "item");

            serializer.startTag(null, "titulo");
            serializer.attribute(null, "fecha", news.get(i).getPubDate());
            serializer.text(news.get(i).getTitle());
            serializer.endTag(null,"titulo");

            serializer.startTag(null, "enlace");
            serializer.text(news.get(i).getLink());
            serializer.endTag(null,"enlace");

            serializer.startTag(null, "descripcion");
            serializer.text(news.get(i).getDescription());
            serializer.endTag(null,"descripcion");

            serializer.endTag(null, "item");
        }
        serializer.endTag(null, "titulares");
        serializer.endDocument();
        serializer.flush();
        fout.close();
    }

    public static void createJSON(List<Item> news, String fileName) throws IOException, JSONException {
        OutputStreamWriter out;
        File file;
        JSONObject object, rss;
        JSONArray jsonArray;

        file = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fileName);
        out = new FileWriter(file);
        //crear objeto JSON
        object = new JSONObject();
        object.put("web", "http://www.europapress.es/");
        object.put("link", "http://www.europapress.es/rss/rss.aspx?ch=00279");
        jsonArray = new JSONArray();
        for (int i = 0; i < news.size(); i ++) {
            JSONObject item = new JSONObject();
            item.put("titular", news.get(i).getTitle());
            item.put("enlace", news.get(i).getLink());
            item.put("fecha", news.get(i).getPubDate());
            item.put("descripcion", news.get(i).getDescription());
            jsonArray.put(item);
        }
        object.put("titulares", jsonArray);
        rss = new JSONObject();
        rss.put("rss", object);
        out.write(rss.toString(5)); //tabulación de 5 caracteres
        out.flush();
        out.close();
        Log.i("info", object.toString());
    }

    public static void createGSON(List<Item> news, String file) throws IOException {
        OutputStreamWriter out;
        File miFichero;
        Headline headline;
        String texto;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setDateFormat("dd-MM-yyyy");
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();

        headline = new Headline();
        headline.setWeb("http://www.europapress.es/");
        headline.setFeed("http://www.europapress.es/rss/rss.aspx?ch=00279");
        headline.setHeadlines(news);
        texto = gson.toJson(headline);


        miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), file);
        out = new FileWriter(miFichero);
        out.write(texto);
        out.close();
    }
}
