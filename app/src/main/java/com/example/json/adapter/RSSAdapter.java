package com.example.json.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.json.R;
import com.example.json.model.Article;
import com.example.json.model.Repo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by paco on 6/02/18.
 */

public class RSSAdapter extends RecyclerView.Adapter<RSSAdapter.ViewHolder> {
    private ArrayList<Article> articles;

    public RSSAdapter(){
        this.articles = new ArrayList<>();
    }

    public Article getItem(int position) {
        return articles.get(position);
    }

    public void setArticles(ArrayList<Article> articles) {
        this.articles = articles;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView) TextView textView;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View repoView = inflater.inflate(R.layout.rss_item_view, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(repoView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Article article = articles.get(position);

        holder.textView.setText(article.getTitle());
   }

    @Override
    public int getItemCount() {
        return articles.size();
    }
}
