package com.example.json.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.json.R;
import com.example.json.model.Repo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReposAdapter extends RecyclerView.Adapter<ReposAdapter.ViewHolder> {

    private ArrayList<Repo> repos;

    public ReposAdapter() {
        this.repos = new ArrayList<>();
    }

    public Repo getItem(int position){
        return repos.get(position);
    }

    public void setRepos(ArrayList<Repo> repos) {
        this.repos = repos;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textView1) TextView tvName;
        @BindView(R.id.textView2) TextView tvDescription;
        @BindView(R.id.textView3) TextView tvCreatedAt;

        public  ViewHolder(View itemView){
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public ReposAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View repoView = inflater.inflate(R.layout.item_view, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(repoView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ReposAdapter.ViewHolder viewHolder, int position) {

        Repo repo = repos.get(position);

        viewHolder.tvName.setText(repo.getName());
        viewHolder.tvDescription.setText(repo.getDescription());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        viewHolder.tvCreatedAt.setText(dateFormat.format(repo.getCreatedAt()));
        //viewHolder.tvCreatedAt.setText( repo.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return repos.size();
    }
}
