package com.example.json.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.json.R;
import com.example.json.model.Contact;
import com.example.json.network.RestClient;
import com.example.json.utils.Analysis;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import cz.msebera.android.httpclient.Header;

public class ContactsActivity extends AppCompatActivity {
    //private static final String WEB="http://raul.alumno.mobi/acceso/json/contactos.json";
    //private static final String WEB="http://raul.alumno.mobi/acceso/json/contactosnull.json";
    private static final String WEB="http://192.168.103.113/acceso/contactos.json";
    //private static final String WEB="https://paco.alumno.mobi/json/contactos.json";
    //private static final String WEB="https://sergio.alumno.mobi/json/contactos.json";
    //ArrayList<Contact> contacts;
    ArrayAdapter<Contact> adapter;

    @BindView(R.id.listView) ListView listView;

    @OnItemClick(R.id.listView)
    public void onItemClick(int position) {
        //showMessage("Móvil: " + contacts.get(position).getPhone().getMobile());
        showMessage("Móvil: " + adapter.getItem(position).getPhone().getMobile());
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button)
    public void download(View view){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        RestClient.get(WEB, new JsonHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                // called before request is started
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Connecting . . .");
                //progreso.setCancelable(false);
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progressDialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                progressDialog.dismiss();
                try {
                    adapter.clear();
                    adapter.addAll(Analysis.contactsAnalyze(response));
                } catch (JSONException e) {
                    e.printStackTrace();
                    showMessage("Error:\n" + e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progressDialog.dismiss();;
                StringBuilder message = new StringBuilder("Error:\n");
                if (responseString != null)
                    message.append(responseString + '\n');
                if (throwable != null)
                    message.append(throwable.getMessage());
                showMessage(message.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressDialog.dismiss();;
                StringBuilder message = new StringBuilder("Fallo:\n");
                if (errorResponse != null)
                    message.append(errorResponse.toString())
                            .append('\n');
                if (throwable != null)
                    message.append(throwable.getMessage());
                showMessage(message.toString());
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        ButterKnife.bind(this);
        adapter = new ArrayAdapter<Contact>(this, android.R.layout.simple_list_item_1, new ArrayList<Contact>());
        listView.setAdapter(adapter);
    }
}
