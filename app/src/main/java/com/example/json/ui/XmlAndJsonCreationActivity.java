package com.example.json.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.json.R;
import com.example.json.model.pojo.Item;
import com.example.json.model.pojo.RssJson;
import com.example.json.network.RestClient;
import com.example.json.utils.Analysis;
import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import fr.arnaudguyon.xmltojsonlib.XmlToJson;

public class XmlAndJsonCreationActivity extends AppCompatActivity {
    public static final String WEB = "https://www.europapress.es/rss/rss.aspx?ch=00279";
    //public static final String WEB = "https://paco.alumno.mobi/json/rss_europapress.xml";
    public static final String XML_FILE = "resultado.xml";
    public static final String JSON_FILE = "resultado.json";
    public static final String GSON_FILE = "resultado_gson.json";
    private static final int REQUEST_CONNECT = 1;
    RssJson rssJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xml_and_json);

        ButterKnife.bind(this);
    }

    @OnClick(R.id.button)
    public void download(View view) {
        final ProgressDialog progressDialog = new ProgressDialog(this);

        //usar TextHttpResponseHandler()
        RestClient.get(WEB, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Conectando a . . ." + '\n' + WEB);
                progressDialog.setCancelable(true);
                progressDialog.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progressDialog.dismiss();
                StringBuilder message = new StringBuilder("Error: " + statusCode + "\n");
                if (throwable != null)
                    message.append(throwable.getMessage());
                showMessage(message.toString());
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                XmlToJson xmlToJson;
                Gson gson;

                progressDialog.dismiss();
                xmlToJson = new XmlToJson.Builder(responseString)
                        .forceList("/rss/channel/item/category")
                        .build();
                //JSONObject jsonObject = xmlToJson.toJson();
                //Log.i("json", xmlToJson.toString());
                try {
                    gson = new Gson();
                    rssJson = gson.fromJson(xmlToJson.toString(), RssJson.class);
                    if (checkPermission())
                        create(rssJson.getRss().getChannel().getItem());
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                    showMessage("error en la conversión:\n" + e.getMessage());
                }
            }
        });
    }

    private void create(List<Item> items) {
        try {
            Analysis.createXML(items, XML_FILE);
            showMessage("Fichero XML creado: " + XML_FILE);
            Analysis.createJSON(items, JSON_FILE);
            showMessage("Fichero JSON creado: " + JSON_FILE);
            Analysis.createGSON(items, GSON_FILE);
            showMessage("Fichero GSON creado: " + GSON_FILE);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    private boolean checkPermission() {
        String permission = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        boolean granted = false;
        // comprobar los permisos
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{permission}, REQUEST_CONNECT);
            // Cuando se cierre el cuadro de diálogo se ejecutará onRequestPermissionsResult
        } else {
            granted = true;
        }
        return granted;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[],
                                           int[] grantResults) {
        String permiso = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        // chequeo los permisos de nuevo
        if (requestCode == REQUEST_CONNECT)
            if (ActivityCompat.checkSelfPermission(this, permiso) == PackageManager.PERMISSION_GRANTED)
                // permiso concedido
                create(rssJson.getRss().getChannel().getItem());
            else
                // no hay permiso
                showMessage("No se ha concedido el permiso");
    }
}
