package com.example.json.ui;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.json.R;
import com.example.json.network.RestClient;
import com.example.json.utils.Analysis;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;

public class LotteryActivity extends AppCompatActivity {
    private static final String WEB="https://paco.alumno.mobi/json/primitiva.json";
    //private static final String WEB="https://paco.alumno.mobi/json/primitivamal.json";

    @BindView(R.id.textView) TextView textView;
    @OnClick(R.id.button)
    public void download(View view){
        final ProgressDialog progressDialog = new ProgressDialog(this);
        RestClient.get(WEB, new JsonHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                // called before request is started
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setMessage("Connecting . . .");
                //progreso.setCancelable(false);
                progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progressDialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                progressDialog.dismiss();
                try {
                    textView.setText(Analysis.lotteryAnalyze(response));
                } catch (JSONException e) {
                    e.printStackTrace();
                    textView.setText("Error:\n" + e.getMessage());
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                super.onFailure(statusCode, headers, responseString, throwable);
                progressDialog.dismiss();;
                StringBuilder message = new StringBuilder("Error:\n");
                if (responseString != null)
                    message.append(responseString + '\n');
                if (throwable != null)
                    message.append(throwable.getMessage());
                textView.setText(message);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                progressDialog.dismiss();;
                StringBuilder message = new StringBuilder("Fallo:\n");
                if (errorResponse != null)
                    message.append(errorResponse.toString())
                            .append('\n');
                if (throwable != null)
                    message.append(throwable.getMessage());
                textView.setText(message);
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lottery);

        ButterKnife.bind(this);
    }
}
