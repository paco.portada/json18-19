package com.example.json.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.example.json.R;
import com.example.json.adapter.ClickListener;
import com.example.json.adapter.RecyclerTouchListener;
import com.example.json.adapter.ReposAdapter;
import com.example.json.model.Repo;
import com.example.json.network.ApiRestClient;
import com.example.json.network.ApiService;

import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepositoriesActivity extends AppCompatActivity implements Callback<ArrayList<Repo>>{

    //public static final String BASE_URL = "https://api.github.com/";

    ApiService apiService;

    private ReposAdapter adapter;

    @BindView(R.id.editText) EditText editText;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    @OnClick(R.id.button)
    public void download(View view) {
        String userName = editText.getText().toString();
        if (userName.isEmpty())
            showMessage("Debe dar un nombre");
        else {
            hideSoftKeyboard();
            //poner cuadro de progreso
            //Retrofit
            //Call<ArrayList<Repo>> call = apiService.listRepos(userName);
            Call<ArrayList<Repo>> call = ApiRestClient.getInstance().listRepos(userName);
            //Call<ArrayList<Repo>> call = ApiRestClient.getInstance().getRepos();
            call.enqueue(this);
        }
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT ).show();
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager)
                    getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repositories);

        ButterKnife.bind(this);

        adapter = new ReposAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //manage click
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                showMessage("Single Click on position:" + position);
                Uri uri = Uri.parse((String) adapter.getItem(position).getHtmlUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                if (intent.resolveActivity(getPackageManager()) != null)
                    startActivity(intent);
                else
                    showMessage("No hay un navegador");
            }

            @Override
            public void onLongClick(View view, int position) {
                showMessage("Long press on position :" + position);
            }
        }));
        //Retrofit

    }

    @Override
    public void onResponse(Call<ArrayList<Repo>> call, Response<ArrayList<Repo>> response) {
        if (response.isSuccessful()) {
            adapter.setRepos(response.body());
            showMessage("Repositorios actualizados ok");
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Error en la descarga: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<ArrayList<Repo>> call, Throwable t) {
        StringBuilder message = new StringBuilder();
        message.append("Fallo en la comunicación:\n");
        if (t != null)
            message.append(t.getMessage());
        showMessage(message.toString());
        }

}
