package com.example.json.ui;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.example.json.R;
import com.example.json.adapter.ClickListener;
import com.example.json.adapter.RSSAdapter;
import com.example.json.adapter.RecyclerTouchListener;
import com.example.json.model.RSSFeed;
import com.example.json.network.XmlApiAdapter;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsActivity extends AppCompatActivity implements Callback<RSSFeed> {

    private RSSAdapter adapter;
    //private RSSFeed rssFeed;
    //private ArrayList<Article> articles;

    //ApiService apiService;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @OnClick(R.id.button)
    public void download(View view) {

        //hideSoftKeyboard();
        //poner cuadro de progreso
        Call<RSSFeed> call = XmlApiAdapter.getInstance().loadRss();
        call.enqueue(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        ButterKnife.bind(this);

        adapter = new RSSAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //manage click
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this, recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                //Values are passing to activity & to fragment as well
                showMessage("Single Click on position:" + position);
                //Uri uri = Uri.parse((String) repos.get(position).getUrl());
                Uri uri = Uri.parse((String) adapter.getItem(position).getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                if (intent.resolveActivity(getPackageManager()) != null)
                    startActivity(intent);
                else               
                    showMessage("No hay un navegador");
            }

            @Override
            public void onLongClick(View view, int position) {
                showMessage("Long press on position :" + position);
            }
        }));

        //retrofit

    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(Call<RSSFeed> call, Response<RSSFeed> response) {
        if (response.isSuccessful()) {
            //rssFeed = response.body();
            //articles = (ArrayList<Article>) rssFeed.getArticleList();
            try {
                adapter.setArticles(response.body().getArticleList());
                showMessage("Noticias actualizadas ok");
            } catch (Exception e) {
                showMessage("Error: Lista de noticias vacía");
            }
        } else {
            StringBuilder message = new StringBuilder();
            message.append("Error en la descarga: " + response.code());
            if (response.body() != null)
                message.append("\n" + response.body());
            if (response.errorBody() != null)
                try {
                    message.append("\n" + response.errorBody().string());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            showMessage(message.toString());
        }
    }

    @Override
    public void onFailure(Call<RSSFeed> call, Throwable t) {
        t.printStackTrace();
        if (t != null)
            showMessage("Fallo en la comunicación:\n" + t.getMessage());
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

}

